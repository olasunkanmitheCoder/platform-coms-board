import micropython; micropython.alloc_emergency_exception_buf(100)
from machine import UART
from pyb import LED, Accel
import time
import spacecan
import spacecan.pus


uart = UART(6, 9600, timeout=100)
uart_buffer = bytearray()
accel = Accel()
heartbeat_led = LED(1)
loop_led = LED(3)
update_led = LED(2)


responder = spacecan.Responder(node_id=1, hb_period=0.5, max_hb_miss=5)
pus = spacecan.pus.PacketUtilizationService(responder)
responder.connect(interface="pyboard", channel_a=1, channel_b=2)
responder.start()


def hearbeat_received():
    heartbeat_led.toggle()


responder.heartbeat.callback = hearbeat_received


# print on the screen all incoming packets
def packet_monitor(packet, node_id):
    print(f"{packet.service}, {packet.subtype} from {node_id}")


# function ids
CMD_GPS_SUBUNIT_OFF = 1
CMD_GPS_SUBUNIT_ON = 2


def perform_function_handler(packet, node_id):
    function_id = packet.data[0]
    if function_id == CMD_GPS_SUBUNIT_OFF:
        print("GPS now off")
    elif function_id == CMD_GPS_SUBUNIT_ON:
        print("GPS now on")
    else:
        return False
    return True


pus.packet_monitor = packet_monitor
pus.function_management_service.callback = perform_function_handler


# parameter names
XYZ001 = 1
XYZ002 = 2
XYZ003 = 3
XYZ004 = 4
XYZ005 = 5
XYZ006 = 6
XYZ007 = 7

pus.parameter_management_service.init_parameter_pool(
    # name      description     encoding    value   validity
    (XYZ001,    "valid",        "B",        0,      None),
    (XYZ002,    "time",         "d",        0,      1),
    (XYZ003,    "latitude",     "f",        0,      1),
    (XYZ004,    "longitude",    "f",        0,      1),
    (XYZ005,    "acc_x",        "f",        0,      None),
    (XYZ006,    "acc_y",        "f",        0,      None),
    (XYZ007,    "acc_z",        "f",        0,      None),
)

pus.housekeeping_service.create_housekeeping_report_structure(
    structure_id=1,
    interval=5,
    enabled=True,
    parameter_ids=[XYZ001, XYZ002, XYZ003, XYZ004, XYZ005, XYZ006, XYZ007]
)
# use a macro name
update = pus.parameter_management_service.set_parameter_value


try:
    while True:
        time.sleep(0.1)
        loop_led.toggle()

        # read from UART
        lines = []
        while uart.any():
            lines.append(uart.readline())

        # process lines
        for line in lines:
            if line and line.startswith("$GPRMC"):
                line = line.decode()
                _, *data = line.split(",")
                if len(data) > 9:
                    print(line)
                    update_led.toggle()
                    _valid = 1 if data[1] == "A" else 0
                    _time = float(data[0])
                    update(XYZ001, _valid)
                    update(XYZ002, _time)
                    if _valid:
                        _latitude = float(data[2]) * (1 if data[3] == "N" else -1)
                        _longitude = float(data[4]) * (1 if data[5] == "E" else -1)
                        update(XYZ003, _latitude)
                        update(XYZ004, _longitude)

        # read acceleration
        _accel = accel.filtered_xyz()
        update(XYZ005, _accel[0])
        update(XYZ006, _accel[1])
        update(XYZ007, _accel[2])

        # send period housekeeping
        pus.housekeeping_service.run()

except KeyboardInterrupt:
    pass

responder.stop()
responder.disconnect()
